package ru.zolov.tm.comparator;

import java.util.Comparator;
import ru.zolov.tm.entity.AbstractGoal;

public class DateStartComparator implements Comparator<AbstractGoal> {

  @Override
  public int compare(AbstractGoal a1, AbstractGoal a2) {
    return a1.getDateOfStart().compareTo(a2.getDateOfStart());
  }

}
