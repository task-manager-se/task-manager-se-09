package ru.zolov.tm.comparator;

import java.util.Comparator;
import ru.zolov.tm.entity.AbstractGoal;

public class DateCreateComparator implements Comparator<AbstractGoal> {

  @Override
  public int compare(AbstractGoal a1, AbstractGoal a2) {
    return a1.getDateOfCreate().compareTo(a2.getDateOfCreate());
  }
}
