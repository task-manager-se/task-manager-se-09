package ru.zolov.tm.comparator;

import java.util.Comparator;
import ru.zolov.tm.entity.AbstractGoal;

public class StatusComparator implements Comparator<AbstractGoal> {

  @Override
  public int compare(AbstractGoal a1, AbstractGoal a2) {
    return a1.getStatus().compareTo(a2.getStatus());
  }
}
