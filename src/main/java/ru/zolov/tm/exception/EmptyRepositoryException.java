package ru.zolov.tm.exception;

public class EmptyRepositoryException extends Exception {

  public EmptyRepositoryException() {
    super("Repository is empty!");
  }
}
