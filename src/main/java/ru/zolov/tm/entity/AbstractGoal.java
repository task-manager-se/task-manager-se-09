package ru.zolov.tm.entity;


import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import ru.zolov.tm.enumerated.StatusType;

@Getter
@Setter
public class AbstractGoal extends AbstractEntity {

  protected String userId;
  protected String name = "";
  protected String description = "empty";
  protected Date dateOfStart;
  protected Date dateOfFinish;
  protected Date dateOfCreate = new Date();
  protected StatusType status = StatusType.PLANNED;
}
