package ru.zolov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractGoal implements Comparable<Task> {

  @NotNull private String projectId;

  @Override
  public int compareTo(@NotNull Task task) {
    int c = this.getStatus().compareTo(task.getStatus());
    if (c == 0) return this.getId().compareTo(task.getId()); return c;
  }
}
