package ru.zolov.tm.entity;

import java.util.UUID;
import lombok.Getter;

@Getter
public abstract class AbstractEntity {

  protected String id = UUID.randomUUID().toString();
}
