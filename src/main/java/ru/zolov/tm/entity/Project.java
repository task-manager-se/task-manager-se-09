package ru.zolov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Project extends AbstractGoal implements Comparable<Project> {

  @Override
  public int compareTo(@NotNull Project project) {
    int c = this.getStatus().compareTo(project.getStatus());
    if (c == 0) return this.getId().compareTo(project.getId()); return c;
  }
}
