package ru.zolov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum CompareType {
  DATECREATE("date-create", "Sorted by create date"),
  DATESTART("date-start", "Sorted by start date"),
  DATEFINISH("date-finish", "Sorted by finish date"),
  STATUS("status", "Sorted by status");

  private String name;
  private String description;

  CompareType(@NotNull final String name, @NotNull final String description) {
    this.name = name; this.description = description;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return name;
  }
}
