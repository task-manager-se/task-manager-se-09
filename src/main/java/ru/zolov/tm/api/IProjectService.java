package ru.zolov.tm.api;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectService {

  IProjectRepository getProjectRepository();

  ITaskRepository getTaskRepository();

  Project create(@Nullable String userId,
                 @Nullable String name,
                 @Nullable String description,
                 @Nullable String start,
                 @Nullable String finish) throws EmptyStringException, ParseException;

  Project read(@Nullable String userId, @Nullable String id)
  throws EmptyStringException, EmptyRepositoryException;

  List<Project> readAll(@Nullable String userId)
  throws EmptyStringException, EmptyRepositoryException;

  void update(@Nullable String userId,
              @Nullable String id,
              @Nullable String name,
              @Nullable String description,
              @Nullable String start,
              @Nullable String finish) throws ParseException;

  boolean remove(@Nullable String userId, @Nullable String id)
  throws EmptyStringException, EmptyRepositoryException;

  List<Project> sortBy(@Nullable final String userId, @Nullable Comparator<AbstractGoal> comparator)
  throws EmptyRepositoryException;

  List<Project> findProject(@Nullable String userId, @Nullable String partOfTheName)
  throws EmptyRepositoryException, EmptyStringException;
}
