package ru.zolov.tm.api;

import java.util.List;
import ru.zolov.tm.command.AbstractCommand;

public interface ServiceLocator {

  IProjectService getProjectService();

  ITaskService getTaskService();

  ITerminalService getTerminalService();

  List<AbstractCommand> getCommandList();

  IUserService getUserService();
}
