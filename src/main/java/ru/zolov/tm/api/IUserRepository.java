package ru.zolov.tm.api;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.User;

public interface IUserRepository {

  User persist(@NotNull User user);

  List<User> findAll();

  User findOne(@NotNull String id);

  User findByLogin(@NotNull String login);

  void merge(@NotNull User user);

  boolean remove(@NotNull String id);

  void removeAll();

  void update(@NotNull String id, @NotNull String login);
}
