package ru.zolov.tm.api;

import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectRepository {

  Project persist(@NotNull Project project);

  Project findOne(@NotNull String userId, @NotNull String id)
  throws EmptyStringException, EmptyRepositoryException;

  List<Project> findAll(@NotNull String userId) throws EmptyRepositoryException;

  void update(@NotNull String userId,
              @NotNull String id,
              @NotNull String name,
              @NotNull String description,
              @NotNull Date start,
              @NotNull Date finish);

  boolean remove(@NotNull String userId, @NotNull String id);

  void removeAll(@NotNull String userId) throws EmptyRepositoryException;

  void merge(@NotNull Project project);

  List<Project> findProject(@NotNull String userId, @NotNull String partOfTheName)
  throws EmptyRepositoryException;
}
