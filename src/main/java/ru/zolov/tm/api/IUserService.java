package ru.zolov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.exception.UserExistException;
import ru.zolov.tm.exception.UserNotFoundException;

public interface IUserService {

  User getCurrentUser();

  void setCurrentUser(@Nullable User currentUser);

  boolean isAuth();

  User login(@Nullable String login, @Nullable String password)
  throws EmptyStringException, UserNotFoundException;

  void userRegistration(@Nullable String login, @Nullable String password)
  throws EmptyStringException, UserExistException;

  void adminRegistration(@Nullable String login, @Nullable String password)
  throws EmptyStringException;

  void logOut();

  boolean isRolesAllowed(@Nullable RoleType... roleTypes);

  User findByLogin(@Nullable String login) throws EmptyStringException;

  User findById(@Nullable String id) throws EmptyStringException, UserNotFoundException;

  boolean remove(@Nullable String id) throws EmptyStringException;

  void updateUserPassword(@Nullable String id, @Nullable String password)
  throws EmptyStringException;
}
