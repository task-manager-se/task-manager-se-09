package ru.zolov.tm.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.api.ITaskRepository;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

  @Override
  public Task persist(@NotNull final String userId,
                      @NotNull final String projectId,
                      @NotNull final String name) {
    final Task task = new Task(); task.setUserId(userId); task.setProjectId(projectId);
    task.setName(name); storage.put(task.getId(), task); return task;
  }

  @Override
  public Task persist(@NotNull final Task task) {
    storage.put(task.getId(), task);
    return task;
  }

  @Override
  public Task findOne(@NotNull final String userId,
                      @NotNull final String projectId,
                      @NotNull final String id)
  throws EmptyStringException, EmptyRepositoryException {
    final Task task = storage.get(id); if (task == null) throw new EmptyRepositoryException();
    if (!task.getUserId().equals(userId)) throw new EmptyStringException();
    if (!task.getProjectId().equals(projectId)) throw new EmptyStringException(); return task;
  }


  @Override
  public List<Task> findAll(@NotNull final String userId) throws EmptyRepositoryException {
    final List<Task> taskList = new ArrayList<>(); for (final Task task : storage.values()) {
      if (task == null) throw new EmptyRepositoryException();
      if (task.getUserId().equals(userId)) taskList.add(task);
    } return taskList;
  }

  @Override
  public List<Task> findTaskByProjId(@NotNull final String userId, @NotNull final String projectId)
  throws EmptyRepositoryException {
    final List<Task> result = new ArrayList<>(); for (final Task task : storage.values()) {
      if (task == null) throw new EmptyRepositoryException();
      boolean match = task.getProjectId().equals(projectId) && task.getUserId().equals(userId);
      if (match) result.add(task);
    } return result;
  }

  @Override
  public void update(@NotNull final String userId,
                     @NotNull final String id,
                     @NotNull final String name,
                     @NotNull final String description,
                     @NotNull final Date start,
                     @NotNull final Date finish) {
    final Task task = storage.get(id); if (task.getUserId().equals(userId)) {
      task.setName(name); task.setDescription(description); task.setDateOfStart(start);
      task.setDateOfFinish(finish);
    }
  }

  @Override
  public boolean remove(@NotNull final String userId, @NotNull final String id) {
    final Task task = storage.get(id);
    if (task.getUserId().equals(userId)) {
      storage.remove(id);
      return true;
    }
    return false;
  }

  @Override
  public void removeAll(@NotNull final String userId) throws EmptyRepositoryException {
    for (Task task : findAll(userId)) {
      storage.remove(task);
    }
  }

  @Override
  public boolean removeAllByProjectID(@NotNull final String userId, @NotNull final String id) {
    for (final Task task : storage.values()) {
      if (task.getUserId().equals(userId) && task.getProjectId().equals(id)) {
        storage.remove(task.getId()); return true;
      }
    } return false;
  }

  @Override
  public void merge(@NotNull final Task task) {
    if (storage.containsKey(task.getId())) {
      update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateOfStart(), task
          .getDateOfFinish());
    }
    else persist(task);
  }

  @Override
  public List<Task> findTask(@NotNull final String userId, @NotNull final String partOfTheName)
  throws EmptyRepositoryException {
    @NotNull final List<Task> resultList = new ArrayList<>();
    for (@NotNull final Task task : findAll(userId)) {
      if (task.getName().contains(partOfTheName) || task.getDescription().contains(partOfTheName)) {
        resultList.add(task);
      }
    } return resultList;
  }
}


