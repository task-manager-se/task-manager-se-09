package ru.zolov.tm.command.system;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public class SearchCommand extends AboutCommand {

  private final String name = "search";
  private final String description = "Search project and task by part of the name.";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute()
  throws CommandCorruptException, EmptyRepositoryException, EmptyStringException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    System.out.println("Enter part of the project or task name: ");
    @NotNull final String partOfTheName = serviceLocator.getTerminalService().nextLine().trim();
    @NotNull final List<Project> projectList = serviceLocator.getProjectService()
                                                             .findProject(userId, partOfTheName);
    @NotNull final List<Task> taskList = serviceLocator.getTaskService()
                                                       .findTask(userId, partOfTheName);
    if (partOfTheName.isEmpty()) System.out.println("The search criteria you entered is empty.");

    serviceLocator.getTerminalService().performList(projectList, userName);
    serviceLocator.getTerminalService().performList(taskList, userName);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.USER, RoleType.ADMIN};
  }
}
