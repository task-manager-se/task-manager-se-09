package ru.zolov.tm.command.task;

import java.util.Comparator;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class TaskDisplayCommand extends AbstractCommand {

  private final String name = "task-list";
  private final String description = "Display task of list";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute()
  throws EmptyStringException, EmptyRepositoryException, CommandCorruptException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    final List<Project> listOfProjects = serviceLocator.getProjectService().readAll(userId);
    serviceLocator.getTerminalService().performList(listOfProjects, userName);
    System.out.print("Enter project id: ");
    final String projectId = serviceLocator.getTerminalService().nextLine();
    System.out.println("Please choose sort type. Enter: \n date-create \n date-start \n date-finish \n status");
    final String comparatorName = serviceLocator.getTerminalService().nextLine();
    Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService()
                                                        .getComparator(comparatorName);
    if (comparator == null) throw new CommandCorruptException();
    final List<Task> list = serviceLocator.getTaskService().sortBy(userId, projectId, comparator);
    serviceLocator.getTerminalService().performList(list, userName);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
