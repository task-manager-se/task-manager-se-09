package ru.zolov.tm.command.task;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class TaskRemoveCommand extends AbstractCommand {

  private final String name = "task-remove";
  private final String description = "Remove task";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute() throws EmptyStringException, EmptyRepositoryException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    @NotNull final List<Task> listOfTasks = serviceLocator.getTaskService().readAll(userId);
    serviceLocator.getTerminalService().performList(listOfTasks, userName);

    System.out.print("Enter task id: ");
    final String id = serviceLocator.getTerminalService().nextLine();
    System.out.println(serviceLocator.getTaskService().remove(userId, id));
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
