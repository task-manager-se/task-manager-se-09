package ru.zolov.tm.command.project;

import java.text.ParseException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class ProjectEditCommand extends AbstractCommand {

  private final String name = "project-edit";
  private final String description = "Edit project";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute() throws ParseException, EmptyRepositoryException, EmptyStringException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    final List<Project> list = serviceLocator.getProjectService().readAll(userId);
    serviceLocator.getTerminalService().performList(list, userName);
    System.out.print("Enter project id: ");
    final String projectId = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter project name: ");
    final String projectName = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter new description: ");
    final String projectDescription = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project start in format DD.MM.YYYY: ");
    final String start = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project finish in format DD.MM.YYYY: ");
    final String finish = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getProjectService()
                  .update(userId, projectId, projectName, projectDescription, start, finish);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
