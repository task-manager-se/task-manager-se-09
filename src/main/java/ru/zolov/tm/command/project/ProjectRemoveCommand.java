package ru.zolov.tm.command.project;

import java.util.List;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class ProjectRemoveCommand extends AbstractCommand {

  private final String name = "project-remove";
  private final String description = "Remove project";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute() throws EmptyStringException, EmptyRepositoryException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    @NotNull final String userName = serviceLocator.getUserService().getCurrentUser().getLogin();
    @NotNull final List<Project> list = serviceLocator.getProjectService().readAll(userId);
    serviceLocator.getTerminalService().performList(list, userName);
    System.out.print("Enter project id: ");
    @NotNull final String projectId = serviceLocator.getTerminalService().nextLine();
    System.out.println(serviceLocator.getProjectService().remove(userId, projectId));
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
