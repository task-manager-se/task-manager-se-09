package ru.zolov.tm.command.project;

import java.text.ParseException;
import org.jetbrains.annotations.NotNull;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.enumerated.RoleType;
import ru.zolov.tm.exception.CommandCorruptException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public final class ProjectCreateCommand extends AbstractCommand {

  private final String name = "project-create";
  private final String description = "Create new project";

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public boolean secure() {
    return false;
  }

  @Override
  public void execute()
  throws CommandCorruptException, EmptyStringException, EmptyRepositoryException, ParseException {
    @NotNull final String userId = serviceLocator.getUserService().getCurrentUser().getId();
    System.out.print("Enter project name:");
    final String projectName = serviceLocator.getTerminalService().nextLine();
    System.out.print("Enter description: ");
    final String projectDescription = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project start in format dd.MM.yyyy: ");
    final String start = serviceLocator.getTerminalService().nextLine();
    System.out.println("Enter date of project finish in format dd.MM.yyyy: ");
    final String finish = serviceLocator.getTerminalService().nextLine();
    serviceLocator.getProjectService()
                  .create(userId, projectName, projectDescription, start, finish);
  }

  @Override
  public RoleType[] roles() {
    return new RoleType[]{RoleType.ADMIN, RoleType.USER};
  }
}
